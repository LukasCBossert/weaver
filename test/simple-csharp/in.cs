// <!--
// SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>
// 
// SPDX-License-Identifier: CC0-1.0
// -->

// # A C# Example Code

// Use some includes to have access to libraries.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
The thing is that we might want to have multi
line comments here! This is very new.
*/
namespace CSharpTutorials
{
    class Program
    {
        static void Main(string[] args)
        {
            string message = "Hello World!!";

            Console.WriteLine(message);
        }

        static void Test(){
            // Multi line string with `$"`
            return $"
                {2+2}
            ";
        }
    }
}

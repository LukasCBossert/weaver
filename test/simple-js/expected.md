<!--
SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>

SPDX-License-Identifier: CC0-1.0
-->

# An Example JavaScript File

JavaScript allows multi line strings using backticks:
```js
var s = `
Das ist ein \`text\`.
`;

```

Of course also multi line comments can work. and should
be rendered accordingly.


```js
document.getElementById('a').text = s;

```
## A loop

sometimes we just loop around.

```js
for(var i=0; i<100; i++){ console.log(i); }
```

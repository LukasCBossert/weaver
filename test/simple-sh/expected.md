
# A Wondeful Shell Test

Lets print a simple string
```sh
echo "Hello World"

```
Check someting interesting
```sh
if [ ! -d "." ]; then
    echo "Wow. The current directory is a directory"
fi

```
This is about multi line strings:
```sh
echo "
    Multi line strings in Shell mode
    # This is not a comment!
"
```

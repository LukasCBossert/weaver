#!/bin/sh

# <!--
# SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>
#
# SPDX-License-Identifier: CC-BY-SA-4.0 OR MIT
# -->

# # Tests Runner for `weave.awk`

# Based on a set of test cases, the script should create a test report that can 
# be parsed by GitLabs
# [test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html) feature.
# To achieve this, the report has to comply to JUnit XML format.

# ### Test Case Layout

# Test cases should be located in the subfolder `./test`. Each test case
# then consists of a subfolder with three files like that:
# > ```
# >./test/sample-test-case
# > ├── args.txt
# > ├── expected.md
# > └── in.sh
# > ```
# * `in.sh` is the input file that is provided to the script
# * `expected.md` is the expected output to be produced by the script
# * `args.txt` (optional) gives addtional command line args to the script e.g.
#   for enabling feature flags.
# File extensions are ignored by the script.

# ### About JUnit XML

# [JUnit XML](https://www.ibm.com/docs/en/adfz/developer-for-zos/14.1.0?topic=formats-junit-xml-format) 
# format was originally designed for reporting test results in the Java
# programming language ecosystem. I became a de-facto standard for reporting
# test cases of other programming languages and most testing frameworks have
# converters or output formatters for this  format. You can find a JUnit XML
# Schema
# [here](https://github.com/windyroad/JUnit-Schema/blob/master/JUnit.xsd).
# GitLab offers to parse JUnit XML files to create graphical report e.g. in 
# pipelines or in merge requests.

# ## Global Variables

# ### Script File

# The path to the script being tested. Set a default if not set in the
# environment.
SCRIPT_FILE=${SCRIPT_FILE:-"./lib/weave.awk"}

# ### Test Path

# The path where test cases are stored. Set a default if not set in the
# environment.
TEST_PATH=${TEST_PATH:-"./test"}

# ### Report File

# The path to the file used to store the JUnit formatted report. Set a default
# if not set in the environment.
REPORT_FILE=${REPORT_FILE:-"report.xml"}

# ### Failure Status

# Record the overall failture status of the test cases. If a single test case
# failed this is set to `1`.
FAIL=0

# ## Helper Functions

# To streamline the output of the test report we define a set of helper
# functions.

# ### Write Line to Report

# Write a single line to the report.
out(){
    echo "$1" >> $REPORT_FILE
}

# ### Write Header

# Write the XML header elements according to the JUnit XML schema.
out_header(){
    # When writing a header, we assume that we start a new file, so make it
    # empty first.
    truncate -s 0 "$REPORT_FILE"
    out '<?xml version="1.0" encoding="UTF-8" ?>'
    out '<testsuites id="'"$(date -Iseconds)"'" name="Unit Tests for '"$SCRIPT_FILE"'">'
    out '  <testsuite id="'"$TEST_PATH"'" name="Tests from '"$TEST_PATH"'">'
}

# ### Write Test Case

# Write the result of a single test case. If `$2` is not empty we assume that
# the test case failed and use the contents as the error message. `$3` is used
# for the detailed error description.
out_testcase(){
    out '    <testcase id="'"$1"'" name="Test Case '"$1"'">'
    if [ -n "$2" ]; then
        out '      <failure message="'"$2"'" type="ERROR">'
        out '<![CDATA['
        out "$3"
        out ']]>'
        out '      </failure>'
    fi
    out '    </testcase>'
}

# ### Write Footer

# Write the XML footer elements according to the JUnit XML schema.
out_footer(){
    out '  </testsuite>'
    out '</testsuites>'
}

# ## Installing Prerequisites

# on some container images `find` and `diff` are is missing (e.g. `fedora`). 
# Make sure to note that and try to install them:
if [ ! -x "$(command -v find)" ] || [ ! -x "$(command -v diff)" ]; then
    echo "WARN: Could not find at least one of 'find' or 'diff'."
    # Lets check which package manager is available on the system and then use
    # that to install the required packages.
    if [ -x "$(command -v yum)" ]; then
        echo "  trying to install using 'yum'."
        yum install -y findutils diffutils > /dev/null
    else
        # Aw, snap! The current environment does not provide find and does not 
        # support any of the package managers listed above; Abort the process.
        echo "ERROR: Could install. Aborting."
        exit 1
    fi
fi
# ## Test Execution

# First make sure that the script is executable.
chmod +x ./lib/weave.awk

# We will need some temporary files to store the results of the test cases.
# Lets create them now:
TMP_DIFF=$(mktemp)
TMP_ERR=$(mktemp)
TMP_ACTUAL=$(mktemp)

# Before starting the actual work, write the report header.
out_header

# Go through all files (`-type f`) startting with `tc-` (for "*t*est *c*ase")
# in the directory `./test`.
for TESTCASE in $(find "./test/" -mindepth 1 -maxdepth 1 -type d); do
    echo "Running '$TESTCASE'"
    # Make sure that we always start "clean" so truncate temporary files from
    # previous test cases.
    truncate -s 0 "$TMP_DIFF"
    truncate -s 0 "$TMP_ERR"
    truncate -s 0 "$TMP_ACTUAL"

    TC_IN=$(find "$TESTCASE" -maxdepth 1 -type f -name "in.*")
    TC_EXPECTED=$(find "$TESTCASE" -maxdepth 1 -type f -name "expected.*")
    TC_ARGS=$(find "$TESTCASE" -maxdepth 1 -type f -name "args.*")

    # Check if the required files are available. Otherwise report the test as
    # failed.
    if [ ! -f "$TC_IN" ]; then
        FAIL=1
        echo "  failed."
        out_testcase "$TESTCASE" "No input file found" ""
        continue
    fi

    if [ ! -f "$TC_EXPECTED" ]; then
        FAIL=1
        echo "  failed."
        out_testcase "$TESTCASE" "No expected file found" ""
        continue
    fi

    # Check if additional arguments are given for the test case
    if [ -f "$TC_ARGS" ]; then
        # Run the script for the file, but also pass the command line
        # arguments from the args file. Save the result in a temporary file for
        # evauliation.
        ARGS="$(cat "$TC_ARGS" | tr -d '\r?\n') $TC_IN"
        echo "$ARGS" | xargs "$SCRIPT_FILE" 2> "$TMP_ERR" > "$TMP_ACTUAL"
    else
        # Run the script for the file, save the result in a temporary file for
        # evauliation.
        "$SCRIPT_FILE" "$TC_IN" 2> "$TMP_ERR" > "$TMP_ACTUAL"
    fi

    # If `awk` did not exit cleanly, the script likely did not run at all.
    # Report the error that `awk` gave and report the test case as failed.
    if [ "$?" != 0 ] ; then
        FAIL=1
        echo "  failed."
        out_testcase "$TESTCASE" "Parsing error" "$(cat "$TMP_ERR")"
        continue
    fi

    # Compare the expected to the actual output in the temporary file created
    # before.
    diff -du "$TC_EXPECTED" "$TMP_ACTUAL" 2> "$TMP_ERR" > "$TMP_DIFF"

    # If diff did not exit cleanly, then the files are different and thus the
    # test case failed. Report the failure.
    if [ "$?" != 0 ] ; then
        FAIL=1
        echo "  failed."
        out_testcase "$TESTCASE" "Output does not match expected" "$(cat "$TMP_ERR" "$TMP_DIFF")"
        continue
    fi

    # If we reach here, the test case is passed. Report that to the user.
    echo "  passed."
    out_testcase "$TESTCASE"
done

# Hard work is done. Write the report footer.
out_footer

# Make sure we are not leaving behind any stray files
rm "$TMP_DIFF"
rm "$TMP_ERR"
rm "$TMP_ACTUAL"

# If the overall failure status recorded one test case failing, exit with error
# status.
if [ "$FAIL" = "1" ]; then
    echo "fail"
    exit 1;
fi